---
layout: post
title: Vermögensaufbau mit wenig Zeitaufwand - Mit Daueraufträgen das Hirn entlasten
date: 2019-08-03 08:10:00
header-img: /img/2019/unspash-money-system.jpg
teaser: "Sparen war für mich viele Jahre ein Qual und von Frust, Verzweiflung und Versagensangst geprägt. Doch dann kam der Durchbruch: Ich konnte mein Sparen automatisieren."
category:
  - familienfinanzen
---
### Wen bezahlst du?

Wen genau bezahlst du, wenn:
- Du ein Brötchen kaufst? - Den Bäcker
- Du deine Miete bezahlst? - Den Vermieter
- Dein Auto tankst? - Die Tankstelle

„Endlich ich, Zeit für mich“, sangen Pur für einen Bierwerbespot für über zehn Jahren. Sparen wird oftmals mit Mangel und Schwere gleich gesetzt. Ein für mich völlig neuer Blickwinkel ergab sich beim Lesen von Robert Kiyosakis Klassiker „Rich Dad, Poor Dad.” Er deutet Sparen in Sich-Selbst-Bezahlen um: Pay yourself first.

Damit stellt er das Sparen in ein völlig neues Licht. Es wird damit von einer Mangelaktion zu einem Akt der Selbstliebe. „Ich spare, weil ich es mir wert bin. Und ich spare, soviel ich möchte.“, ist einer der Kernsätze dieses Ansatzes.

### Sparen, was ürbig bleibt?

Die ersten Jahre, nachdem ich auf eigenen Beinen stand, sparte ich, was am Ende des Monats übrig war. Und das war meinst: nix. Und damit bin ich sicherlich nicht alleine. Zu Sparen, was übrig bleibt, funktioniert bei mir einfach nicht. Und da es zu der Zeit eh kaum Zinsen auf Sparguthaben gab, sparte ich nicht mehr und hoffte auf die nächste Gehaltserhöhung, damit ich dann sparen könnte. Schließlich sollte ja mehr Geld zur Verfügung stehen.
Seltsamer Weise stiegen mit meine Einnahmen auch die Ausgaben. [Alex Fischer](https://www.digistore24.com/redir/81229/moerssl/CAMPAIGNKEY) beschreibt diesen Effekt in seinem [Buch](https://www.digistore24.com/redir/81229/moerssl/CAMPAIGNKEY) als „Parkinsonsches Gesetz:“
> „Ausgaben steigen stets bis an die Grenzen des Einkommens.“

### Entscheidungen vermeiden - Sparen mit Dauerauftrag

Bevor ich mich genauer mit dem Thema sparen auseinander gesetzt hatte, habe ich immer auf den Girokonto gespart. Den Erfolg davon hatte ich bereits beschrieben und ein für mich validier Grund ist tatsächlich das Parkinsonsche Gesetz.
Ein erster Schritt war für mich ein separates Konto zum Sparen zu nutzendann zu sparen, wenn noch Geld da ist und dann am besten automatisch per Dauerauftrag.

Mein Spardauerauftrag läuft am Monatsletzten. Da ist meistens das Gehalt bereits auf dem Girokonto und die Fixkosten noch nicht abgebucht. Ich bezahle mich also auch chronologisch zuerst.

### Sparen auf der Überholspur - Sparen mit System

Wenn eins alles bis hierhin umgesetzt hat, dann wandern jeden Monat ein bestimmter Anteil Geld auf ein separates Konto - ein Spar- und Investitionskonto. „Wird man davon reich?“, fragt [Bodo Schäfer in diesem Video](https://www.youtube.com/watch?v=chssfQG_vko). Seine Antwort wird den einen oder anderen vielleicht überraschen, aber sie ist „Nein!“
Aber der erste Money Coach Europas wäre nicht der erste Money Coach Europas, wenn er darauf nicht eine einfache, wie geniale Antwort hätte.

Bodo erwähnt in diesem Video einen Trick, der so einfach, wie genial: Jede Gehaltserhöhung wird zu 50% in die Sparquote gesteckt. Wenn ich also eine anfänglich 50 EUR spare und eine Gehaltserhöhung von 200 EUR pro Monat erhalte, dann ist die Sparquote ab dann 150 EUR.
Warum ist dieser Ansatz so einfach, wie genial? Ab dem Moment, an dem 200 EUR auf dem Konto landen, habe ich mich noch nicht an diese 200 EUR mehr gewöhnt. Das Parkinsonsche Gesetz greift noch nicht! Das Sparen der 100 zusätzlichen Euro tut kein bisschen weh. Selbst mit kleine Beträgen wird ein insgesamt großer Unterschied geschaffen.

### Mein Geschenk für dich

Diese ganzen Infos waren für mich sehr viel Stoff. Und ich wollte umsetzen, doch mit fehlten die Tools, um es mir einfach zu machen und das gelernte auf meien Situation als Familienvater anpassen.
Aus diesem Grund habe ich eine Excel-Vorlage für ein Kontenmodell entwickelt, die mich beim Aufbau und der Wartung dieses Systems unterstützt. Teil dieser Vorlage ist außerdem ein Sparquotenrechner, der den Tipp von Bodo Schäfer aus dem vorigen Absatz berücksichtigt.
Mit dieses Tool nimmt euch nun Denkleistung ab und ihr konnt zusätzlich dazu noch eure Sparquotenerhöhungen validieren. Für mich ist es ein sehr mächtiges Werkzeug geworden.

<div class="cta">
<a href="?geschenk" class="cta cta__button" title="Sparquotenrechner herunterladen" data-track='{"category": "lead", "action": "show-layer-post", "name": "sparsystem"}' toggle-layer>Sparquotenrechner jetzt kostenlos runterladen</a>
</div>

### Sparen, und Wie gehts weiter

Ziel dieses ersten Schrittes ist es, stetig Geld auf einem separaten Konto zu sammeln. Doch nicht nur in Niedrigzinsphasen ist das schiere Sammeln von Geld eine schlechte Idee. Jeder geparkte Euro wird jeden Tag weniger Wert - Inflation sei dank.
Damit es dazu nicht kommt, sollten die gesammtelen Euros investeiert werden. Das ist die zweite Stufe des Systems.

Das Investitionskonto ist nun wiederum der Ausgangspunkt für einen geschlossenen Kreislauf. Von hieraus kann in die verschiedensten Anlageklassen investiert werden:

- Immobilien
- (Dividenden-)Aktien
- Anleihen
- ETFs und andere Fonds
- P2P-Kredite
- Unternehmensanteile
- in das eigene Wissen
