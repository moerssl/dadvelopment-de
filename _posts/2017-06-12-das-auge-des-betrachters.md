---
layout: post
permalink: /2017/06/12/das-auge-des-betrachters/
title: Das Auge des Betrachters
teaser: "
Der Grat zwischen Explosion und Faszination ist für mich manchmal ein schmaler. Es folgt eine kleine Geschichte, in der ich in die Faszination abbog."
tags: [geek]
date: 2017-06-12 12:00:00
header-img: /img/2017/dadvelopment-auge-des-betrachters.jpeg
category:
  - artgerecht
---
Es war an einem Donnerstag. Unsere Familie war noch zu dritt. Die Liebste war zur Weiterbildung beim [Artgerecht Projekt](http://argerecht-projekt.de) in Köln.  Sohn1 und ich hatten vier Tage für uns. Es war an Tag zwei - jener Donnerstag. Vormittags hatten wir uns schon an einem kleinen Grillexperiment versucht und zusammen eine Gewürzmischung hergestellt. Auf unserem Tisch standen noch Gewürze und so weiter rum.

## Ab nach draußen

Alles war zusammen gerühert und nun nach einiger Zeit auch fertig für den Grill. Wir zwei gingen nach draußen, grillen, essen und spielen. Bald war es Zeit für den Bücherbus. Alle zwei Wochen parkt er in Laufreichweite vor unserem Bäcker.
Zusammen mit Nachbars verbrachten wir den Nachmittag am Bücherbus und der deren Wohnung. Ein paar halbe Stunden später machten wir uns auf den Heimweg - zwei Häuser weiter.

Ich schloss die Haustür auf, das mir auffiel, dass die ausgeliehenen Bücher und auch das Laufrad noch in Nachbars Garten standen. Ich mache mich also auf den Weg zurück. Wieder in der Nähe unseres Hauses angekommen, sehe ich, wie ein Gewürzglas in hohem Bogen aus der Haustür auf den Gehweg fliegt. "Nun ja, du hast es schließlich auch stehen lassen", dachte ich mir und ging nach drinnen.

## Was war passiert?

Er hatte ein ganzes Glas Knoblauchpulver auf dem Boden verteilt und damit gespielt. Ich konnte mich nun entscheiden zu schimpfen, dass man ja das Zeug nicht auf den Boden kippt (was auch nicht passiert wäre, hätte ich es weggeräumt) oder das Kunstwerk zu bewundern, dass unser Kind da auf dem Boden geschaffen hat. Tatsächlich sah das Knoblauchpulver für mich wie ein Auge aus. Und genau das war meine Reaktion: "Du hast ja ein Auge auf den Boden gemalt."

![Das Auge des Betrachters](/img/2017/dadvelopment-auge-des-betrachters.jpeg)

## Seine Reaktion überraschte mich

Ich bewunderte noch das Auge (und meine Reaktion auf das verstreute Pulver). Dann dachte ich, ich hörte nicht richtig: "Papa? Wir müssen den Staubsauger holen und das wegsaugen." Unser Sohn, der bisher den Raum wechselte, wenn der Staubsauger lief, wollte nun selbst saugen. Also holte ich das Ding ins Wohnzimmer. Wir schlossen den Strom an und er saugte den Knoblauch selbst weg. Seit dem möchte er öfter Staubsaugen. Eine Entwicklung, die er wahrscheinlich nicht gemacht hätte, wenn ich geschimpft und nicht  das Auge gesehen hätte. Ein schöner Moment.
