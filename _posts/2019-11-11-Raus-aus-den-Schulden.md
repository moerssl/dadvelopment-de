---
title: Raus aus den Schulden - Mit System Schulden abbauen
layout: post
header-img: /img/2019/schulden-abbauen-schneeball-2.jpg
header-img-style: "top: -42px"
teaser: Wie können Kredite schnell abbezahlt werden oder Schulden abgebaut werden. Und was ist überhaupt das Problem mit Schulden?
abstract: Wie können Kredite schnell abbezahlt werden oder Schulden abgebaut werden. Und was ist überhaupt das Problem mit Schulden?
category:
  - familienfinanzen
permalink: raus-aus-den-schulden
redirect_from:
  - /familienfinanzen/2019/11/11/Raus-aus-den-Schulden/
  - /2019/11/11/Raus-aus-den-Schulden/
---
Viele Menschen in Deutschland haben Schulden - und manchmal wissen sie es gar nicht. Mal tarnen sie sich als Dispo, Kreditkarte oder als Null-Prozent-Finanzierung.

Was sie alle gemeinsam haben? Es ist Geld, das uns nicht gehört, für das wir Gebühren bezahlen, weil wir es uns geliehen haben. Diese Gebühr heißt dann Zins - oder in manchen Konstrukten auch Kontoführungsgebühr.

Unterm Strich haben wir Geld ausgegeben, das uns nicht gehört und bezahlen regelmäßig dafür. Man könnte fast von negativem Cashflow sprechen.

## Schulden zurückzahlen per Schneeball

Nun gibt es unterschiedliche Herangehensweisen, um Schulden abzuzahlen. Eine sehr wirksame ist der Schuldenschneeball von Dave Ramsey.

Und so gehts:
- Liste alle deine Schulden nach dem Restbetrag auf
- Sortiere sie nach der Höhe des Restbetrags unabhängig vom Zinsatz
- Reduziere alle Zahlungen auf den Mindestbetrag
- Nutze freies Geld, um den kleinsten Kredit abzuzahlen. Reduziere Ausgaben und erhöhe  Einnahmen, um diesen Betrag so hoch wie möglich ansetzen zu können.
- Ist der Kredit abgelöst, nimm das Geld, dass du bisher für Kredit 1 genutzt hast und addiere es auf die Rate für Kredit 2, bis auch dieser abgelöst ist.
- Wiederhole das bis zur Schuldenfreiheit
- Nutze nun das Geld vom Schuldenabbau für den Vermögensaufbau!

Punkt sieben ist so etwas wie das dreckige Geheimnis beim Schuldenabbau. Arbeitet der Zinseszins beim Schuldenabbau gegen dich, wird er beim Vermögensaufbau dein Verbündeter.

## Die 50:50 Variante

Dave Ramsey hat den Schneeball in seinem Buch "The total money makeover" beschrieben. Der Abbau von Konsumschulden  mit allen Mitteln, ist der zweite von mehreren Babysteps. Der erste ist der Aufbau eines Notfallfonds über 1000 Dollar. Das können auch 1000 Euro sein.

Dave Ramsey plädiert erst später mit dem Investieren anzufangen.

Bodo Schäfer, Europas Moneycoach,  geht in "Der Weg zur finanziellen Freiheit" einen anderen Weg. Er teilt 50:50 auf in Investitionsgeld und Geld, das zum Schuldenabbau genutzt wird.

Während Dave Ramseys Ansatz die Zinszahlungen verringert, möchte Bodo Schäfer das Mindset von reich denkenden Menschen bereits früh durch Taten bestärken. Gleichzeitig habe ein wachsendes Vermögen einen besseren pychologischen Effekt als sinkende Schulden.

## Tools für den Schuldenabbau

Egal welche Variante auch gewählt wird, man benötigt Tools, um die Übersicht zu behalten. Meine Kontenmodellvorlage ist für beide Herangehensweisen geeignet. 
Speziell für den Schulden-Schneeball habe ich eine weitere Excel-Vorlage entwickelt und stelle sie hier zur Verfügung.

<div class="cta">
<a href="https://docs.google.com/spreadsheets/d/1QgQGws5Kq1MMWPzRwc5vdfKS3O7t5hkGRd5vxi8QNvs/edit?usp=sharing" class="cta cta__button" title="Vorlage Schuldenschneeball" data-track='{"category": "lead", "action": "download", "name": "schulden"}' target="_blank">Ja, ich möchte mit dieser Vorlage durchstarten</a>
</div>
