---
layout: post
permalink: /schnelle-entscheidung-72-stunden-regel
title: "72 Stunden Regel: Von Frost, einem Beil und einer schnellen Entscheidung"
teaser: "Wie mein großer Sohn mich mal wieder beeindruckte - und ich ein paar Minuten später mit einem Beil im Garten stand. Und was wir von ihm lernen können."
description: "Wie mein großer Sohn mich mal wieder beeindruckte - und ich ein paar Minuten später mit einem Beil im Garten stand. Und was wir von ihm lernen können."
date: 2018-02-25 12:00:00
header-img: /img/2018/schnelle-entscheidung-dadvelopment.jpg
category:
  - vom-kind-gelernt
---
Er hat es wieder einmal geschafft mich nachhaltig zu beeindrucken. Und ein "Das will ich auch"-Gefühl in mir zu wecken. Ich spreche vom großen Sohn. Was genau war geschehen?

Wir sitzen an einem kalten Sonntag Nachmittag auf dem Sofa. Unser Gesprächsthema kommt auf unsere Zahnstellungen zu sprechen. Und darauf, dass der Schnuller für den Großen nicht gut für seine Kieferstellung ist. Das wissen wir seit einem Zahnarztbesuch genauer.

Die Liebste und ich berichten von unseren Erfahrungen mit den Zahnspangen und davon, dass es sein Körper noch schaffen kann, den Überbiss zu korrigieren, wenn er seinen "Mimi", wie er den Schnuller nennt, auch nachts abgeben würde. Soweit war es nur ein Gespräch.

"Der Mimi soll nicht an den Schnullerbaum, dann werfen die den weg. Und das will ich nicht", sagt er. "Ich will den lieber in meiner Piratenkiste vergraben.", sprach er. Stand auf und sammelte seine Schnuller ein. Zusammen mit der Liebsten malte er noch eine Schatzkarte. Kaum waren zehn Minuten vergangen, standen wir mit Schaufel und Spaten im Garten. Bei minus zwei Grad und gefrorenem Boden.

Ich selbst war total geflasht von der Reaktion des Großen. *Kaum hatte er die Entscheidung getroffen, war er auch schon mitten drin in der Umsetzung.* Diese tolle Erfahrung wollte ich ihm auch nicht durch gefrorenen Boden nehmen lassen. In Ermangelung einer frostgeeigneten Hacke griff ich zum Beil. Wahrscheinlich halten uns die Nachbarn nun für verrückt - aber vielleicht auch nicht erst seit jetzt. Wenn das Kind auch bei Bodenfrost ein Loch im Garten haben möchte, um seinen Piratenschatz zu vergraben, dann graben wir ein Loch. Da lasse ich mich nicht durch Wetter aufhalten.

Was nun genau nehme ich mit? Er hat eine für ihn sehr schwierige Entscheidung getroffen. Er hat sie schnell getroffen und sofort gemäß dieser Entscheidung gehandelt. Er hat die [72-Stunden-Regel](https://karrierebibel.de/72-stunden-regel/) angewandt, ohne sie zu kennen. Diese Regel besagt, dass:
 * Dinge, die man sich vornimmt innerhalb von 72 Stunden begonnen werden müssen, sonst
 * sinkt die Chance der Umsetzung auf unter ein Prozent

**Wenn ich mir nun etwas vornehme, denke ich an meinen Sohn, der auch sofort gehandelt hat.**
