---
layout: post
title: Christian Bischoffs Unaufhaltbar - Lesechallenge 2019 #1
date: 2019-03-07 08:00:00
permalink: /christian-bischoff-unaufhaltbar-lesechallenge
header-img: /img/2019/unaufhaltbar.jpg
teaser: Unaufhaltbar von Christian Bischoff ist mehr als nur ein Buch. Es ist eine Bauanleitung für ein Leben nach den eigenen Vorstellungen.
---
## Unaufhaltbar - Anders als andere Bücher

Schon der erste Blick ins Buch zeigte mir, dass es sich bei Christian Bischoffs Buch um ein etwas anderes Buch handeln wird. Die Überschriften sind bunt, die Absätze kurz. Als Hörer von Christians Podcast hatte ich beim Lesen seine Stimme im Ohr. Für mich war es ein Lesepodcast.
Wer gern an sich arbeitet, für den ist dieses Buch genau richtig. Hier werden nicht nur Phrasen gedroschen. Hier gehts ans Eingemachte. Aber dazu später mehr.

### Der Sieg wird im Kopf entschieden

Christian Bischoff war Deutschlands jüngster Basketballbundesliga Coach und hatte auch vorher schon lange Jahre Erfahrung im Profisport. Diese Einstellung kommt zwischen den Zeilen durch. Ich spürte beim Lesen, dass er das Beste für jeden seiner Leser*innen möchte. Manche Aussagen sind unbequem, andere Fragen und Aufgaben herausfordernd. Und Christian spricht Klartext. In der Mitte des ersten ersten Kapitels schreibt er z.B.

__Mach diese Übung JETZT!__ <br /> __(Erst dann weiterlesen. :-) )__

### Mind Building - Das Body Building für das Selbstbewusstsein

Im zweiten Kapitel gehts um die Stärkung des Selbstbewusstseins. Diese Buchabschnitt seziert nach und nach Selbstzweifel aus unseren Erinnerungen. Obwohl es Selbstzweifel nicht gibt, wie Christian Bischoff am Anfang dieses Kapitels darstellt.
Nach und nach landen verschiedene Quellen von Zweifeln auf dem Tisch, werden betrachtet und entlarvt. Allein dieses Kapitel ist es wert gelesen zu werden.

### Über Geld spricht man

Die ersten zwei Kapitel drehen sich um das Innen. Unser Denken und unser Fühlen. Im abschließenden Kapitel des 170 Seiten Buches geht es um Finanzen. Christian zeigt anschaulich durch das Bild eines Finanzflusses, wie Menschen [finanzielle Unabhängigkeit erreichen](https://www.digistore24.com/redir/206131/moerssl/CAMPAIGNKEY) können.
Dieses Kapitel zeigt interessierte Personen, wie man stetig finanzielle Ziele erreicht. Und zwar nicht um wahllos Geld anzuhäufen, sondern um Freiheit zu erleben.

### Christian verschenkt sei Buch

Ich habe Christians Buch "Unaufhaltbar" recht früh gekauft und mit Bedacht gelesen. Die Fragen, Darstellungen und Übungen hatten bereits kurz nach dem Lesen einen Einfluss auf meine Entscheidungen. Ich kann dieses Buch guten Gewissens empfehlen. Christian Bischoff ist nicht nur ein guter Redner und Trainer, sondern auch ein guter Autor. Neben "Unaufhaltbar" hat er bereits andere Bücher, u.a. über Selstvertrauen verfasst.
Das tolle an diesem Buch: Christian verschenkt das Buch, man braucht nur die Kosten für Herstellung und Versand zu tragen.

<div class="cta">
<a href="https://www.copecart.com/products/c5ffadcc/p/moerssl" class="cta cta__button cta__button--heavy" title="Unaufhaltbar Geschenk sichern" target="_bla nk">Jetzt das Buch sichern</a>
</div>
