---
layout: post
title: Zinseszins berechnen - Den Zinseszins Effekt berechnen und nutzen
date: 2019-11-10 08:00:00
header-img: /img/2019/zinseszins-berechnen-ausbreitung-auf-zeit.jpg
abstract: Zinseszins berechnen - Wie nutzt man den Zinseszins - das mächtigste Tools im Vermögensaufbau - für sich. Der Zinseszins Rechner visualisiert den Zinseszinseffekt
category:
  - familienfinanzen
---

<zinseszins-berechnen titel="Zinseszins online berechnen: Der Sparplan" start="500" dauer="50" zins="6" raten='[{"jahr": 1, "betrag": 50}]'></zinseszins-berechnen>

## Die Macht des Zinseszins - Reiskornparabel

Um die Erfindung des Schachbretts rankt sich eine Legende. Dieser Legende nach war der Herrscher über dieses Spiel so erfreut, dass er dem Erfinder einen Wunsch erfüllen wollte. Dieser wünschte sich Reis auf dem Schachbrett. Auf dem ersten Feld ein Korn, auf dem zweiten zwei, auf dem dritten vier und immer so weiter. Das nächste Feld sollte immer doppelt so viele Reiskörner erhalten, wie das Feld davor.
Der Herrscher wurde ärgerlich und sagte, er solle sich seinen Sack Reis holen.
Diese Legende ist ein klassisches Beispiel für eine Exponentialrechnung - und wie schwer sich das unser Gehirn vorstellen kann. Dieses Video zeigt eine Visualisierung bezogen auf Deutschland:

<center><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/KnQZ3Mg6upg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Das Video zeigt wie ich finde gut, was sich das menschliche Gehirn zur schwer vorstellen mag. Exponentielles Wachstum ist schwer zu begreifen. Und gleichzeitig besser vorstellbar, wenn es visuell vor Augen ist.

Daher habe ich eine schnelle Visualisiungsmöglichkeit, um Zinseszins in unterschiedlichen Begebenheiten zu berechnen und darzustellen. Jetzt habe ich noch einige Beispiele aus dem echten Leben rausgesucht, um die Kraft hinter dem Zinseszins noch besser darzustellen

## Zinseszins berechnen mit Einmalanlage

Dieses Bespiel dreht sich um die Einmalanlage. Das heißt, es wird zu Beginn einer Periode eine Summe z.B. in einen wiederanlegenden Fonds investiert und dann über mehrere Jahrzehnte nicht angerührt.

<zinseszins-berechnen titel="Einmalanlage: Zinseszins berechnen" start="5000" dauer="50" zins="6" raten='[{"jahr": 1, "betrag": 0}, {"jahr": 5, "betrag": 0}]'></zinseszins-berechnen>

Schon diese einmalige Anlage zeigt nach der Berechnung des Zinseszinses, wie sich die Kraft der Wiederanlage mit dem Lauf der Zeit ergibt. Einmal angelegt, liegen gelassen und die Gewinne reinvestiert, werden aus 5000EUR in 50 Jahren xxx. EUR

## Zinseszins für einen Sparplan berechnen

Der Sparplan wird auch in Deutschland immer bliebter. Mit einem Dauerauftrag wird zu einem bestimmten Zeitpunkt im Monat ein Betrag in Aktien oder Indexfonds investiert. Dieser Ansatz hat ähnliche Effekte, wie die Einmalanlage, jedoch erreicht sie durch die stetige Einzahlung einen weitaus höheren Wert, als die Einmalanlage:

<zinseszins-berechnen titel="Sparplan: Zinseszins berechnen" start="5000" dauer="50" zins="6" raten='[{"jahr": 1, "betrag": 100}]'></zinseszins-berechnen>

## Zinseszins mit wachsender Sparrate berechnen

Schneidet schon der Sparplan mit einer festen Rate beim Zinseszinseffelt sehr viel besser ab als die Einmalanlage, so kann auch dieser noch getoppt werden: Durch eine wachsende Sparrate. Wird diese regelmäßg erhöht, in dem z.B. Gehaltserhöhungen oder Mehrverdienste der Sparrate zu Gute kommen, so ist das wie eine Art Turbo im Vermögensaufbau:

<zinseszins-berechnen titel="Wachstumssparen: Zinseszins berechnen" start="5000" dauer="50" zins="6" raten='[{"jahr": 1, "betrag": 100}, {"jahr": 3, "betrag": 200},{"jahr": 5, "betrag": 400},{"jahr": 10, "betrag": 1000}]'></zinseszins-berechnen>

## So funktioniert Zinseszins

Was genau ist eigentlich dieser Zinseszins? Und warum ist der so mächtig, dass Albert Einstein ihn sogar als die mächtigste Kraft im Universum bezeichnete.
Zins an sich ist recht einfach erklärt. Eine Menge Geld bzw. Kapital (K) erzielt auf ein Jahr gesehen einen weiteren Betrag (Z). Diesen Betrag nennt man "Zinsen" oder auch Ertrag.
Um den Zinseszinseffekt im nächsten Jahr nutzen zu können, nimmt man den anfänglichen Betrag und den Ertrag des ersten Jahres und legt ihn erneut an. Somit erzielen die Zinsen aus dem ersten Jahr im zweiten Jahr selbst Zinsen - die Zinseszinsen.
Nutzt man diesen Effekt über mehrere Jahre hinweg, erhält man ein exponentielles Wachstum. Ähnlich wie bei einer Schneekugel, die schnell immer größer wird.

### Die Formel für (Jahres-)Zins

Zinsen = Kapital * Zinssatz / 100

### Die Formel für Zinseszins

Gesamtkapital = Anfangskapital * Zinssatz ^ Laufzeit in Jahren

## Zinseszins im Anlagesystem - Der geschlossene Kreislauf

Nun zum praktischen Teil. Dass der Zinseszinseffekt sehr mächtig ist, konnten wir in den vergangenen Absätzen bereits erkennen. Wie können wir den nun praktisch für uns nutzen. Der wichtigste Grundsatz hierbei ist:

> Stets reinvestieren

Wenn das investierte Geld Erträge abwirft - die Investments also erträglich sind - sollten die erzielten Erträge auch wieder investiert werden. Ich spreche hier auch gern von einem geschlossenen Kreislauf.
Im Artikel zum automatisierten Sparen bin ich bereits auf diesen Kreislauf eingegangen. Hier kurz rekapituliert:

 1) Bezahle dich stets zuerst
 2) Setze eine Dauerauftrag von deinem Girokonto auf ein Investitionskonto auf.
 3) Investiere vom Investitionskonto in Anlagen, die du verstehst
 4) Reinvestiere Erträge

Wenn du z.B. in deinem Depot ETFs besitzt, so könne sie entweder thesaurierend oder ausschüttend sein. Thesaurierend bedeutet soviel wie "anhäufen" oder "horten". Speziell auf ETFs übertragen: Gewinne und Dividenden werden direkt wieder angelegt - genau der Effekt, den wir haben wollen.
Nun gibt es auch noch ausschüttende Fonds. Diese geben die Dividenden aus - überweisen sie also aufs Investitionskonto. Hält man sich an __Stets reinvestieren__ gehören diese Erträge erneut angelegt.

## Zinseszins mit Kleinsterträgen

> Rule No. 1: Never lose money. Rule No. 2: Never forget rule No. 1. - Warren Buffett

Verliere niemals Geld. Das ist DIE Grundregel von Investmentlegende Warren Buffet. Übertragen auf den geschlossenen Investitionskreislauf kann man seine Regel auch so deuten: __Gib kein Geld für unnötige Gebühren aus__.

Einzelinvestments in Aktien und Fonds kosten in der Regel Transaktionsgebühren. Ich habe gelernt, ETF-Anteile bei meinem Broker bei kleinerem Volumen nur über den Sparplan zu kaufen. Das Order-Entgelt ist mir für kleine Einzelinvestments zu hoch.

Doch was mache ich nun, wenn mir mein ETF eine Auszahlung in kleiner ein- oder zweistelliger Höhe beschert?
Mein Weg aus dieser Misere sind P2P-Kredite. Bei meinem Anbieter kann ich gebührenfrei investieren und das bereits ab 10EUR.

## Zinseszins bei Kreditschulden nutzen

Ähnlich wie bei der Anlage, so kickt der Zinseszinseffekt auch beim Abzahlen von Krediten rein. Ein kleiner Geldbetrag zu Beginn der Rückzahlung oder auch Sondertilungen können einen großen Unterschied in der Gesamtsumme oder auch den zu zahlenden Zinsen machen.


<zinseszins-berechnen titel="Anuitätendarlehen: Zinseszins berechnen" start="300000" dauer="30" zins="2" raten='[{"jahr": 1, "betrag": -1250}, {"jahr": 2, "betrag": -2000},{"jahr": 3, "betrag": -2000},{"jahr": 10, "betrag": -1250}]'></zinseszins-berechnen>

## Zinseszins berechnen: Praxisbeispiel Rauchen 

Jetzt kommt diese ganz fiese Masche, die in so vielen Finanzbüchern auch drin ist. Wie man mit kleinen oder auch großen Änderungen in den Gewohnheiten ein Vermögen machen kann. Dieses Beispiel ist mal das tägliche Bäckerbrötchen, der Kaffee beim Bäcker oder das gekaufte Mittagessen mit den Kollegen.
Zu meine Raucherzeiten brachte ich es auf eine Schachtel am Tag. Das waren damals um die fünf Euro pro Schachtel:

*  5EUR pro Tag
* 35EUR pro Woche
* ca. 140EUR pro Monat

Was passiert nun in den nächsten 30 Jahren, wenn ich mich dazu entscheide 140EUR statt in Nikotin in meine finanzielle Freiheit zu investieren:


<zinseszins-berechnen titel="Zinseszins berechnen: Der Zinseszinseffekt für Nichtraucher" start="0" dauer="30" zins="6" raten='[{"jahr": 1, "betrag": 140}]'></zinseszins-berechnen>


{% include zinseszinsrechner-setup.html %}
