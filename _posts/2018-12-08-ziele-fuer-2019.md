---
layout: post
permalink: /ziele-aufschreiben-2019
title: Ziele aufschreiben - Meine Ziele für 2019
teaser: Werden Ziele niedergeschrieben, dann ist es wahrscheinlicher, dass sie erreicht werden, als wenn man sie "nur" im Kopf hat. Daher gibts hier niedergeschriebene Ziele.
description: Werden Ziele niedergeschrieben, dann ist es wahrscheinlicher, dass sie erreicht werden, als wenn man sie "nur" im Kopf hat. Daher gibts hier niedergeschriebene Ziele für 2019.
date: 2018-12-08 08:00:00
header-img: /img/2019/ziele-aufschreiben-2019.jpg
---
Ab Mitte November beginnt für mich Zeit in zurückzublicken und neue Pläne zu machen. Das ist seit Jahren ein Prozess, da ich Mitte November Geburtstag habe und somit kurz vor Jahresende schon ein Jahresende ansteht.
Für 2019 möchte ich mir nun ganz bewusst Ziele vornehmen und sie mit euch teilen.

## Aus Feedback lernen: Es gibt keine Fehler

Ich möchte trainieren Fehler nicht Fehler, sondern als Feedback zu sehen. Dafür werde ich mir einen spaßigen Rahmen setzen: Darts spielen. Ich möchte dieses Jahr mindestens zwei Mal die Höchstpunktzahl 180 Punkte beim Dart werfen. Und dabei mich an meinen Geburtstagsgeschenk freuen.

## Den eigenen Wert steigern, um meinen Mitmenschen helfen zu können

Dafür nehme ich mir vor mindestens 12 Bücher über Persönlichkeitsentwicklung und Finanzen lesen:
- [Mit Kindern wachsen von Daniela Blickhan](https://amzn.to/2L2bNeb)
- [Unaufhaltbar von Christian Bischoff](https://www.copecart.com/products/c5ffadcc/p/moerssl)
- [Unbox your life von Tobias Beck](https://amzn.to/2SyHX3s)
- [Selbstbewusst in die Gehaltsverhandlung von Burak Kalman](https://www.digistore24.com/redir/197543/moerssl/CAMPAIGNKEY)
- [Rich Dad Poor Dad von Robert Kiyosaki](https://amzn.to/2SA4DQV)
- [More than Money von Andreas Enrico Brell](https://amzn.to/2SBOfj3)
- [Stärken Stärken von Alexander Christiani](https://amzn.to/2QopVVm)
- [Money von Tony Robbins](https://amzn.to/2L2UdqE)
- [Online Marketing Praxishandbuch von Thomas Klußmann](https://www.digistore24.com/redir/137145/moerssl/CAMPAIGNKEY)
- [Das 24 Stunden Buch von Thomas Klußmann](https://www.digistore24.com/redir/164557/moerssl/)
- [Design Thinking Playbook](https://amzn.to/2zNXzJv)
- [Sieger zweifeln nicht von Dirk Kreuter](https://www.digistore24.com/redir/181477/moerssl/CAMPAIGNKEY)

Seminare bzw. Konferenzen besuchen:
- [Die Immobilienoffensive](https://www.digistore24.com/redir/157013/moerssl/CAMPAIGNKEY)
- [Die Contra](https://www.digistore24.com/redir/225996/moerssl/CAMPAIGNKEY)

## Mit meinen Erfahrungen selbst Wert schaffen

In den vergangenen Jahren habe ich im Job, in der Familie und auch im Raum dazwischen Erfahrungen gesammelt. Die wollen schon seit längerer Zeit in die Welt hinaus und 2019 soll das Jahr werden, in denen sie es endlich dürfen. Die Challenge: Jede Woche ein Blogpost.
Dieses Ziel ist erreicht, wenn ich **52 neue Artikel** veröffentlicht habe.
Ich möchte mit diesem Magazin in 2019 monatlich mindestens 3000 Besucher durchschnittlich erreichen. Das Ziel ist also erreicht bei **36000 Unique Visits** erfüllt.

## Mehrere Einkommensquellen erschließen

Damit unsere familiäre Versorgung nicht nur von Zeit-In-Geld-Tausch abhängt, sollen dieses Jahr neue Einkommensquellen erschlossen werden. Wie genau das aussehen wird, weiß ich noch nicht. Jedenfalls sollte es etwas mit stetigem Cashflow und wenig Aufwand sein.
