---
layout: post
status: publish
published: true
title: Ciao WordPress, hallo Jekyll
date: '2017-02-15 00:00:00'
date_gmt: '2017-02-15 00:00:00'
teaser: "
Ein Technikwechsel geht durch den Blog. Er hat sich hinter den Kulissen schon länger angekündigt. Warum ich nun mit Jekyll unterwegs bin, erkläre ich euch im Artikel.
"
description: Von WordPress zu Jekyll zu wechseln ist ein großer Schritt. Marcel hat ihn gewagt und erzählt von seinen Gründen.
tags: [geek]
---

Der Blog läuft nun mit Jekyll. Es ist nicht das erste Mal, dass ich versuche mich durch einen Technologiewechsel zu mehr Bloggen zu bewegen.
Kurze Zeit lief der Blog mit Drupal, bevor ich dann doch wieder zurück zu Wordpress ging.

Nach fast zehn Jahren, kehre ich WordPress nun den Rücken zu. Ich habe meine Habseligkeiten aka Artikel mitgenommen und den Blog nun als statische Seite aufgebaut.
Mein Entwicklerherz konnte nach dem Wechsel zu Neuland nicht mehr mit der "Geschwindigkeit" von WordPress-Seiten. Also versuchte ich vieles, um meinen Blog schneller laden zu lassen.
Das ging auch mehr oder minder gut. Also das Ergebnis. Die Frickelei unter der Haube hat mich dann doch gestört.

Ich wollte etwas anders. Ich wollte Freiheit. Ich wollte meine Seite so aussehen lassen, wie ich sie möchte. Ich wollte nicht erst umständlich neue Felder in der Datenbank anlegen. Nach Möglichkeit wollte ich ein, zwei Anpassungen in ein paar Textdateien machen und das Ergebnis sehen. Sorry, Wordpress.

In den vergangen Jahren habe ich Markdown lieben gelernt. Ich wollte meine Blogartikel in Markdown schreiben. Also bin ich nun auf Jekyll unterwegs. Eine neue Technologie. Und damit eine geringere Schwelle zum Schreiben.
