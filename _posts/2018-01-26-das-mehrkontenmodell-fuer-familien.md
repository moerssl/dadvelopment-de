---
title: Das Mehrkontenmodell für Familien
layout: post
date: 2018-01-30 01:00:00
abstract: "Automatisierung hilft mir mit einem Modell aus mehreren Konten Entspannung in unsere Finanzen zu bekommen. Wie wir mit Spaß entspannt sparen und wie auch ihr das schaffen könnt, lest ihr hier."
description: "Das Mehrkontenmodell hilft mir Entspannung in unsere Finanzen zu bekommen. Automatisierung hilft unserer Familie entspannter zu sparen. Weniger Stress ist das Ergebnis."
permalink: /familienfinanzen/mehrkontenmodell-fuer-familien
header-img: /img/2019/kontenmodell-familien.jpg
sticky: true
category:
  - familienfinanzen
---
## Wie ich mit dem Mehrkontenmodell einen besseren und entspannten Umgang mit meinen Finanzen gefunden habe
>  “Wir sparen, was am Ende des Monats übrig bleibt.”<br />“Am Ende des Geldes ist noch so viel Monat übrig.”

Lange Zeit fand ich mich in den beiden Aussagen wieder. Und auch als ich am Anfang des Monats einen festen Betrag auf ein Tagesgeldkonto transferierte, war ich in der zweiten Monatshälfte doch eher angespannt. Das Kind hat einen Wachstumsschub und wir brauchen neue Kleidung? Für mich hieß das Stess, ich sah schwarz. Ein Buch für mich kaufen? Geht nicht, dafür haben wir kein Geld.
### Dem Geld sagen, wo es hin soll
Ich budgetierte, nutzte dafür verschiedene Tools, die mir mit mal mehr und mal weniger Aufwand einen Überblick über den monatlichen Finanzstatus geben sollten. Anfangs noch mit [gnucash](https://www.gnucash.org/) buchte ich einmal in der Woche über zwei Stunden. Später stieg ich auf [Jameica und Hibiscus](https://www.willuhn.de/products/hibiscus/) um und automatisierte. Der Überblick war zwar da, die Entspannung nicht.
> Budgetieren heißt, dem Geld zu sagen, wo es hin soll, statt sich zu wundern, wo es hin ist. <br /> *Dave Ramsey*

Das Gute an der ganzen Sache? Ich hatte den Überblick über unsere regelmäßigen Ausgaben und Einnahmen. Der Haken? Um sich an das Budget zu halten, war eine logische Angelegenheit. Der aktuelle Stand hinterlegt in einem Tool auf einem PC. Das machte es für mich im Alltag zu einem Hindernis, die Ausgaben und den aktuellen Stand zu überblicken.
### Das Mehrkontenmodell führt zu Entspannung
Die Automatisierung der Kontoumsätze hatte bereits zu Entspannung geführt. Das Budget war jedoch zu einer logischen, nur vermindert sichtbaren, Angelegenheit verkommen. Ende 2017 las ich das erste Mal von einem Mehrkontenmodell. Zu Monatsbeginn sollte nicht nur per Dauerauftrag auf ein Sparkonto eingezahlt werden, sondern auch auf ein Spaßkonto. Coole Idee! 
Die Daueraufträge laufen dafür seit ein paar Monaten und mein Zwischenergebnis: Ja, es funktioniert bei mir. Wir zahlen per Dauerauftrag am Monatsersten in den Katastrophenfonds und aufs Spaßkonto ein. Nun sind die Spaßausgaben und auch das Budget dafür einfach einsehbar - per App auf einem Konto. Und ich bin entspannter.


<a href="https://www.awin1.com/cread.php?s=2273804&v=11329&q=349508&r=471887" alt="DKB Broker" style="display: block; text-align: center;">
    <img src="https://www.awin1.com/cshow.php?s=2273804&v=11329&q=349508&r=471887" border="0">
</a>


### Das Mehrkontenmodell für die Familie angepasst
Mit dieser Konstellation gingen einige Wochen ins Land. Ich wusste von Beginn an, dass wir mit noch einige Anpassungen vornehmen müssten. Die erste Version ist nie die endgültige. Nach und nach nahm ich diese Veränderungen mit den Konten vor. Im Folgenden werde ich euch die Konten vorstellen.
#### Das Konsumkonto
Das Konsumkonto ist das gemeine Girokonto. Hier landen von hier aus werden die Rechnungen bezahlt. Miete, Nebenkosten und alle weiteren Kosten gehen von diesem Konto ab. In der Regel gehen auch hier die Gehälter, das Kindergeld und alle weiteren Einnahmen ein. Am Monatsersten wird von hier das Geld per Dauerauftrag auf die übrigen Konten verteilt.
#### Konto für Kleidung
Anfangs waren die Ausgaben für die Kleidung noch mit im Konsumkonto enthalten. Allerdings sind unsere Kleidungsausgaben sehr unregelmäßig, dafür gleich etwas höher. Deshalb sparen wir auf diesem Konto Beträge für Kleidung an.
#### Konto für jährliche Ausgaben
Ähnlich wie der Kleidung verhält es sich mit den jährlichen Ausgaben. Versicherungen, Abos, Mieterschutzbund, Mitgliedsbeiträge. Zusammengerechnet kommt auch hier ein stattlicher monatlicher Betrag zusammen, den man nun nicht mehr aus dem normalen Konsum bestreiten muss.
#### Investitionen
Wir investieren über Sparpläne in Indexfonds an der Börse. Der Dauerauftrag läuft auf ein Depot bei der DKB. Somit können wir langfristig mit einem Anlagehorizont über mehrere Jahrzehnte von der Entwicklung der Börse profitieren. Hierzu wird es demnächst noch einen eigenen Artikel geben.

#### Investieren für die Kinder
Auch die Kinder sollen von profitieren. Die DKB bietet einen u18-Broker an. Für unsere beiden Kinder investieren wir in die gleichen Indexfonds, wie für uns selber.

#### Konto für Projektsparen
Oftmals stehen in der Zukunft bereits eine Anschaffung an, von der wir jetzt schon wissen. Sei es der nächste Umzug, ein Auto oder wie aktuell bei uns ein Saug- und Wischroboter. Da ich auch für größere Anschaffungen keinen Kredit mehr aufnehmen will, landet auch ein Teil des Einkommens im Projektsparkonto.
#### Katastrophenfonds
Vom Katastrophenfonds hatte ich schon eingangs geschrieben. Spülmaschine, Waschmaschine und Kühlschrank gehen ja immer dann kaputt, wenn man es gar nicht braucht.
#### Das Freizeitkonto
Kino, Sushi, Kino, Konzerte. Das Geld auf diesem Konto ist dazu da, um ausgegeben zu werden.
#### Kredit abzahlen
Auch ein laufender Kredit ist im Grunde genommen nix weiter als ein Konto. Für die Planung unseres Geldflusses beachten wir auch dieses. Oft werden bei Krediten Sonderzahlungen neben den normalen Raten gewährt. Um diese auszunutzen, kann man zum Ansparen zunächst ein weiteres Tagesgeldkonto nutzen.
#### Spenden
Spenden, um regelmäßig mit dem erhaltenen Geld Gutes zu tun.
#### Konto für Weiterbildung
Neben unserer Zeit ist Wissen unser wichtigstes Gut. Manche sprechen sogar vom Humankapital. In der Vergangenheit fiel es mir schwer, Geld für Bücher, Kurse, Ausbildungen oder Bildungsurlaube auszugeben. Seitdem wir das Konto für Weiterbildung haben, fällt es mir leichter in unsere Fortbildung zu investerien.
### Überblick dank Tabellenkalkulation
Im Laufe der letzten Monate sind damit viele Konten zusammen gekommen. Um das erhaltene Geld klug und mit Übersicht aufteilen zu können, habe ich mir eine Excel-Datei angelegt. Darin kann ich das Einkommen prozentual auf die Konten verteilen und erhalte Vorschläge, wie hoch die monatliche Betrag ist, der auf die Konten eingezahlt werden sollte. Basierend darauf kann nun der eigentliche Betrag festgelegt werden. Die Kunst besteht nun darin, nicht mehr als 100% des Geldes zu verteilen.
Ist das geschafft, ist sind die Daueraufträge nur noch reine Fleißarbeit.
### Pay yourself first - Bezahle sich zuerst

Das ist der Grundsatz, der hinter diesem Mehrkontenmodell steckt. Dadurch, dass bereits zu Anfang des Monats Geld automatisiert auf für fest gelegte Zwecke verteilt wird, kann das Geld auf dem Kosumkonto ohne schlechtes Gewissen ausgegeben werden. Um euch den Anfang mit dem Mehrkontenmodell zu erleichtern, biete ich die Excel-Datei zum Download an.

<div class="cta">
<a href="?geschenk" class="cta cta__button" title="Mehrkontenmodellrechner herunterladen" data-track='{"category": "lead", "action": "show-layer-post", "name": "kontenmodell"}' toggle-layer>Mehrkontenmodellrechner jetzt kostenlos runterladen</a>
</div>
