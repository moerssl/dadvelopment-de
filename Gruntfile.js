var mozjpeg = require('imagemin-mozjpeg');


module.exports = function(grunt) {
    var destdir = grunt.option('dest') || '_site';
    console.log(destdir);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        imagemin: {
          site: {
            files: [{
              expand: true,                  // Enable dynamic expansion
              cwd: './',                   // Src matches are relative to this path
              src: ['img/**/*.{png,jpg,gif}'],   // Actual patterns to match
              dest: destdir                  // Destination path prefix
            }]
          }
        },
        filerev: {
          options: {
            algorithm: 'md5',
            length: 8,
          },
          images: {
            src: destdir+'/img/**/*.{jpg,jpeg,gif,png,webp}',
          },
          assets: {
            src: destdir+'/{css,js}/*'
          }
        },
        filerev_replace: {
          options: {
            assets_root: './'+destdir
          },
          compiled_assets: {
            src: '{css,js,img}/*'
          },
          views: {
            src: destdir + '/**/*.html'
          }
        },
        uglify: {
            main: {
                src: 'js/<%= pkg.name %>.js',
                dest: 'js/<%= pkg.name %>.min.js'
            }
        },
        less: {
            expanded: {
                options: {
                    paths: ["css"]
                },
                files: {
                    "css/<%= pkg.name %>.css": "less/<%= pkg.name %>.less"
                }
            },
            minified: {
                options: {
                    paths: ["css"],
                    cleancss: true
                },
                files: {
                    "css/<%= pkg.name %>.min.css": "less/<%= pkg.name %>.less"
                }
            }
        },
        banner: '/*!\n' +
            ' * <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
            ' * Copyright <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            ' * Licensed under <%= pkg.license %> (https://spdx.org/licenses/<%= pkg.license %>)\n' +
            ' */\n',
        usebanner: {
            dist: {
                options: {
                    position: 'top',
                    banner: '<%= banner %>'
                },
                files: {
                    src: ['css/<%= pkg.name %>.css', 'css/<%= pkg.name %>.min.css', 'js/<%= pkg.name %>.min.js']
                }
            }
        },
        watch: {
            scripts: {
                files: ['js/<%= pkg.name %>.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                },
            },
            less: {
                files: ['less/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false,
                }
            },
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        '_site/**/*',
                    ]
                },
                options: {
                    watchTask: true,
                    //server: './_site',
                    notify: false,
                    open: false,
                    port: 3001,
                    proxy: 'http://localhost:4000 '
                }
            }
        }
    });


    // Load the plugins.
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-banner');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-filerev-replace');
    grunt.loadNpmTasks('grunt-filerev');
    grunt.loadNpmTasks('grunt-contrib-imagemin');


    // Default task(s).
    grunt.registerTask('default', ['uglify', 'less', 'usebanner']);
    grunt.registerTask('dev', ['default', 'browserSync', 'watch']);
    grunt.registerTask('dev2', ['default', 'browserSync', 'watch']);
    grunt.registerTask('rev', ['imagemin', 'filerev', 'filerev_replace']);
};
