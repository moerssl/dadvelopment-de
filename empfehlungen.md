---
layout: reco
title: Empfehlungen
header-img: img/about-bg.jpg
naviclass: "noheader"
reco:
  -
    title: Bücher
    items:
      -
        title: Nicola Schmidt und Julia Dibbern - Slow Family
        image: https://bindungistbunt.de/user/pages/01.home/_buecher/_slow-family/nicola-schmidt-slow-family-bindung-ist-bunt.jpg
        link: https://www.buch7.de/store/product_details/1026636787?partner=dadvelopment
      - title: Nicola Schmidt - artgerecht Das andere Babybuch
        image: https://bindungistbunt.de/user/pages/01.home/_buecher/_artgerecht-das-andere-babybuch/nicola-schmidt-artgerecht-das-andere-baby-buch-cover-bindung-ist-bunt.jpg
        link: https://www.buch7.de/store/product_details/1024610237?partner=dadvelopment
      -
        title: Robert Kiyosaki - Rich Dad Poor Das
        image: https://ws-eu.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=3898798828&Format=_SL250_&ID=AsinImage&MarketPlace=DE&ServiceVersion=20070822&WS=1&tag=moerssl-21&language=de_DE
        link: https://amzn.to/2ArqmqU
  -
    title: Finanzen
    items:
      -
        title: Geld spielend lernen - Cashflow das Spiel
        image: https://ws-eu.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B081X5W8RP&Format=_SL250_&ID=AsinImage&MarketPlace=DE&ServiceVersion=20070822&WS=1&tag=moerssl-21&language=de_DE
        link: https://amzn.to/2YWJYN5
      -
        title: Christian Bischoff - Onlinekurs zu Finanzen
        link: https://www.digistore24.com/redir/206131/moerssl/CAMPAIGNKEY
        image: /img/finanzen/dadvelopment-finanzen-christian-bischoff.png
      -
        title: Immopreneur ImmobilienOffensive
        link: https://www.digistore24.com/redir/157013/moerssl/CAMPAIGNKEY
        image: /img/finanzen/dadvelopment-immobilienoffensive.jpg


---
# Meine Empfehlungen für euch

Auf dieser Seite habe ich euch einige Produkte zusammen gestellt, die ich euch guten Gewissens empfehlen möchte. 

Unter der Bücher-Rubrik findet ihr Bücher von Menschen, die mein Leben in den letzten 10 Jahren stark geprägt haben. Julia Dibbern und Nicola Schmidt haben unser Familienleben stark geprägt. Zwei wunderbare Menschen. Die Bücher sind mehr als das 1000-fache Wert.

Robert Kiyosaki hat mit Rich Dad Poor Dad mein Verständis zu Finanzen auf ein ganz neues Level gehoben. Das Buch ist sehr leicht zu lesen - für mich war es mehr als ein Mal Bettlektüre. In diesem Buch zeichnet Kiyosaki die Bilder zweier Personen, die sein Verständnis zu Geld geprägt haben - sein eigener Vater und der Vater seines besten Freundes. Dieses Buch ist eine Pflichtlektüre für jeden Menschen mit Ambitionen sein Geld in die eigenen Hände zu nehmen.

Wolltet ihr schon immer einmal finanziell etwas ausprobieren und vorher Speichern? Mit Cashflow dem Spiel habe ihr die Möglichkeit genau das zu tun. Ziel des Spieles ist es aus dem Alltagstrott und dem Leben von Gehaltseingang zu Gehaltseingang zu lösen.