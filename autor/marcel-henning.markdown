---
layout: page
title: "Ich will nicht, dass Papa arbeiten muss!"
header-img: img/ueber-mich/marcel-henning-dadvelopment.jpg
---
Ich bin Marcel und war bei unseren beiden Kindern in Elternzeit. Einmal zwei Monate und einmal sechs Monate.

Als ich mich nach der zweiten Elternzeit wieder auf den Weg in Büro begeben --wollte-- sollte, sagte unser großes Kind diesen Satz:

## Ich will nicht, dass Papa arbeiten muss

Es tat weh. Denn er formulierte genau das, was ich in diesem Moment spürte. Ich möchte bei meiner Familie sein und die Momente mit ihnen genießen.

Also genau das tun, was ich auch die letzten sechs Monate getan hatte.

In mir verfestigte sich der Entschluss, genau darauf hinzuarbeiten:

__Mein Arbeitsleben so zu strukturieren, dass es mein Familienleben unterstützt__

Auf diesem Blog teile ich meine Erfahrungen, meine Tools und meine eigene Entwicklung als Vater. Mein *dadvelopment* auf dem Weg dahin.

### Geld ist Zeit

Als einer der ersten Schritte habe ich unsere Familienfinanzen neu strukturiert, um mit dieser Struktur Kapazitäten und Zeit für die Familie zu haben. Über mehrere Monate und inzwischen sogar Jahre entstand das [Kontenmodell für Familien](/familienfinanzen/mehrkontenmodell-fuer-familien)

Über die Jahre entwickelte es sich zu dem Dreh- und Angelpunkt für unsere Finanzen und unsere Kommunikation über Geld. Ich stelle die Vorlage dafür kostenfrei zur Verfügung:

<div class="cta">
<a href="?geschenk" class="cta cta__button" title="Kontenmodell für Familien" data-track='{"category": "lead", "action": "show-layer-page", "name": "ueber-mich"}' toggle-layer>Kontenmodell für Familien runterladen</a>
</div>

## Meine Reise

Ich bin auf einer Reise hin zu mehr Familienzeit. Dabei begebe ich mich in meine Vergangenheit, plane meine Zukunft, rede mit meiner Familie und spiele mit dem Kindern. Diese Blog soll eine Art Reisetagebuch sein. **Begleitet mich auf meiner Reise.**

