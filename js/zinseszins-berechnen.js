class ZinsesZinsBerechnen extends HTMLElement {

  constructor() {
    super();
    const titel = this.getAttribute("titel") || "Zinseszins berechnen";
    const start = this.getAttribute("start") || 0;
    const zins = this.getAttribute("zins") || 6;
    const dauer = this.getAttribute("dauer") || 30;
    const raten = JSON.parse(this.getAttribute("raten")) || [{jahr: 1, betrag: 100}];

    var app = new Vue({
      el: this,
      components: { VeLine },
      template: `
    <div class="zb-app">
      <h3 class="zb-headline">{{titel}}</h3>
      <div class="zb-inputs">
        <section class="zb-formline">
          <label>Startbetrag:</label>
          <input type="number" v-model="startbetrag"/>EUR
        </section>
        <section class="zb-formline">
          <label>Zinssatz / Rendite:</label>
          <input type="number" v-model="zinssatz"/>%
        </section>
        <!-- section class="zb-formline">
          <label>Monatliche Rate:</label>
          <input type="number" v-model="rate"/>EUR
        </section -->
              <section class="zb-formline">
        <label>Dauer / Anlagehorizont:</label>
        <input type="number" v-model="dauer"/>Jahre
      </section>
        <section class="zb-formline zb-formline--raten">
          <div v-for="(rate, index) in raten" class="zb-raten">
            <label>{{index + 1}}. Ratenanpassung:</label>
            ab Jahr <input type="number" v-model="rate.jahr"/>:
            <input type="number" v-model="rate.betrag"/>EUR
            <button @click="rateEntfernen(rate)" class="zb-button" v-if="index > 0">x</button>
    
          </div>
          <button @click="neueRateAnlegen" class="zb-button">+ weitere Rate</button>
        </section>
      </div>

      <section>
        <ve-line :data="chartData"></ve-line>
      </section>
      <section >
        <button @click="tabelleAnzeigen = !tabelleAnzeigen" class="zb-button">{{werteAnzeigenText}}</button>

        <table v-if="tabelleAnzeigen">
            <tr>
            <th>Jahr</th>
            <th>Rate</th>
            <th>Einzahlung</th>
            <th>Zins</th>
            <th>Summe des Jahres</th>
</tr>
          <tr v-for="jahr in tabelle">
            <td>{{jahr.jahr}}</td>
            <td>{{jahr.rate}}</td>
            <td>{{jahr.eingezahlt}}</td>
            <td>{{jahr.zins}}</td>
            <td>{{jahr.summe}}</td>
          </tr>
        </table>
      </section> 

    </div>
  `,
      data: {
        titel: titel,
        startbetrag: start,
        zinssatz: zins,
        dauer: dauer,
        tabelleAnzeigen: false,
        raten: raten
      },
      methods: {
        neueRateAnlegen: function() {
          let neueRate = {
            jahr: 1,
            betrag: this.rate
          };
          if (this.raten.length > 0)  {
            neueRate.jahr= parseInt(this.raten[this.raten.length-1].jahr)+1;
            neueRate.betrag= this.raten[this.raten.length-1].betrag;
          }
          this.raten.push(neueRate);
        },
        rateEntfernen: function(rate) {
          const index = this.raten.indexOf(rate);
          this.raten.splice(  index, 1);
        }
      },
      computed: {
        werteAnzeigenText() {
          return this.tabelleAnzeigen ? "Berechnungen ausblenden" : "Berechnungen anzeigen";
        },
        tabelle: function() {
          const formatter = new Intl.NumberFormat('de-DE', {
            style: 'currency',
            currency: 'EUR',
            minimumFractionDigits: 2
          })

          let startbetrag = parseFloat(this.startbetrag);
          let rate = parseFloat(this.rate);
          let wertentwicklung = [];
          let einzahlbetrag = startbetrag || 0;
          let summe = startbetrag;
          for (var i = 1; i <= this.dauer; i++) {
            let zins = 0;
            let jahreszins = 0;

            this.raten.map((aenderung) => {
              if (aenderung.jahr <= i) {
                rate = parseFloat(aenderung.betrag);
              }
            });
            for (var j = 1; j <= 12; j++ ) {
              summe = summe + rate;
              zins = summe * (this.zinssatz / 100 / 12);
              summe = summe + zins;
              einzahlbetrag = einzahlbetrag + rate;
              jahreszins = jahreszins + zins;
            }

            wertentwicklung.push({
              eingezahlt: formatter.format(einzahlbetrag),
              raw_einzahlung: einzahlbetrag,
              jahr: i,
              rate: formatter.format(rate),
              zins: formatter.format(jahreszins),
              summe: formatter.format(summe),
              raw_summe: summe
            });

          }
          return wertentwicklung;
        },
        chartData: function() {
          let chartData = {
            columns: ['date', 'Summe', 'Einzahlung'],
            rows: []
          };

          this.tabelle.map((row) => {
            chartData.rows.push({
              date: row.jahr,
              Summe: row.raw_summe,
              Einzahlung: row.raw_einzahlung
            })
          });

          return chartData;
        }
      }
    });
  }

}

document.addEventListener("DOMContentLoaded", function(){
  customElements.define('zinseszins-berechnen', ZinsesZinsBerechnen);
});






