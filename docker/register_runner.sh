# register für kaufen-haupt docker
gitlab-runner register \
    --non-interactive \
    --url="https://gitlab.com/ci" \
    --registration-token="$GITLAB_TOKEN" \
    --executor="docker" \
    --docker-image="docker:latest" \
    --output-limit="40960" \
    --docker-privileged
