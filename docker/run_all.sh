#! /bin/bash

#the current user is `node` set by the Dockerfile on build

#check if a different time zone is given and set it
if [ -n "$TIME_ZONE" ]
then
  echo $TIME_ZONE | sudo tee /etc/timezone;
  sudo dpkg-reconfigure -f noninteractive tzdata;
fi

#set the node version, update the global npm packages, install/update the app's npm and bower packages, run the app in production mode
. ~/.nvm/nvm.sh && nvm use default; \

tail -f $HOME/.pm2/logs/*
