---
layout: post
---
Seit dem letzten Post hier hat sich viel getan. Ich bin noch einmal Vater geworden und habe das Privileg, dass ich ein halbes Jahr Elternzeit nehmen kann.
Ein sehr schönes Gefühl, so viel Zeit für die Familie haben zu können. Passend dazu bin ich vor kurzem auf den Podcast "Der Finanzwesir rockt" gestoßen. Wie genau weiß ich schon gar nicht mehr. Jedenfalls hatte mich der Titel einer Podcastfolge neugierig gemacht. Es ging darum, aus dem Hamsterrad zu entkommen.

Da sich in mir nicht erst seit Beginn der Elternzeit das Gefühl regt, nur noch ungern Lebenszeit gegen Geld einzutauschen, hörte ich in die Folge rein. Und war angefixt. Daniel und Albert sind beide Finanzblogger. Ich stöberte in der nächsten Zeit auf beiden Blog, hörte Podcasts. Und möchte nun den Weg in Richtung passives Einkommen einschlagen. Naja, um weniger Lebenszeit gegen Geld einzutauschen zu müssen.

Albert hat auf seinem Blog einen Plan für die Geldanlage. Den nehme ich mir jetzt mal vor:

## Erste Ebene - Anlagepolitik

> Was sind Ihre Ziele im Leben? Wo soll die Reise hingehen und wie sieht der optimale Mix aus Aktien, Anleihen, Tagegeld und anderen Anlagen aus, um diese Ziele zu erreichen?

Was möchte ich erreichen? Ich möchte weniger Lebenszeit gegen Geld eintauschen und mich langfristig gesehen unabhängig von Lohnarbeit machen. Ich fand schon sehr früh das Konzept des Bedingungslosen Grundeinkommens. Bis es auch in der Politik angekommen, diskutiert und hoffentlich umgesetzt ist, nehme ich nun meine Finanzierung selbst in die Hand.

Ich bin mir bewusst, dass man Geld auch für gute / soziale Dinge einsetzen kann. Das soll sich auch in meiner Anlagepolitik widerspiegeln. Daher werde ich eine Aufteilung in etwa so vornehmen:

- Tagesgeld - für Notfallreserven (3 - 6 Gehälter), Urlaubssparen, Hobbysparen
- ETFs - über Sparpläne
- GLS-Anteile - als risikofreie Anlage

Wagniskapital:
- Crowsinvesting in grüne / soziale Projekte, z.B. auf bettervest.de
- ggf. Anteile an Social Startups mit Gewinnbeteiligung
- P2P-Kredite

## Zweite Ebene - Aktien- und Anleihemix


## Dritte Ebene - Aktiv oder passives

## Vierte Ebene - Produktauswahl

## Fünfte Ebene - Kauf und Verkauf
