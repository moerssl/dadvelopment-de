---
layout: socialmedia
title: 'Links'
header-img: img/about-bg.jpg
naviclass: "noheader"
permalink: links
redirect_from:
  - /socialmedia/
reco:
  -
    title: ''
    items:
      -
        title: Kontenmodellvorlage geschenkt
        link: https://dadvelopment.de/?geschenk
        buttoncss: cta__button--heavy
      -
        title: Das passende Girokonto zum Kontenmodell - vivid
        link: https://vivid.money/r/marcelNAG
      -
        title: Meine Depotempfehlung - Trade Republic (plus 15€)
        link: https://ref.trade.re/gq575bcz
      -
        title: Blog
        link: https://dadvelopment.de
      -
        title: Slow Family - Zutaten für ein für einfache Familienleben        
        link: https://www.buch7.de/store/product_details/1026636787?partner=dadvelopment
      -
        title: Nicola Schmidt - artgerecht Das andere Babybuch       
        link: https://www.buch7.de/store/product_details/1024610237?partner=dadvelopment
      -
        title: Christian Bischoff - Unaufhaltbar
        link: https://www.copecart.com/products/c5ffadcc/p/moerssl
      -
        title: Meine Wunschliste
        link: https://www.amazon.de/hz/wishlist/ls/3HHTFSDCJJJDS?ref_=wl_share


---
